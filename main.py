import pymongo
import random
import time


def create_connection(database):
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client[database]
    return db


def main_menu(db):
    print("======== Thorbe Systems ========")
    print("[                              ]")
    print("[     1· Data management       ]")
    print("[     2· Show events           ]")
    print("[     3· Start event           ]")
    print("[     4· Hall of fame          ]")
    print("[     0· Exit                  ]")
    print("[                              ]")
    print("================================")
    i = input("Select an option (number of list): ")
    if i == "1":
        management_menu(db)

    elif i == "2":
        show_events(db)
    if i == "3":
        i = 3
        start_event(db)
    elif i == "4":
        i = 4
        hall_of_fame(db)

    elif i == "0":
        print("See you later!")
        exit()

    else:
        main_menu(db)


def management_menu(db):
    print("========   MANAGEMENT   ========")
    print("[                              ]")
    print("[     1· List teams            ]")
    print("[     2· Edit team             ]")
    print("[     3· Add new team          ]")
    print("[     4· Create event          ]")
    print("[     0· Main menu             ]")
    print("[                              ]")
    print("================================")
    i = input("You are on GOD MODE now, pls select an option (number of list): ")
    if i == "1":
        list_teams(db)

    elif i == "2":
        x = input("Insert the name of the team: ")
        exists = find_team(db, x)
        if exists:
            edit_team(db, x)

    if i == "3":
        add_team(db)

    if i == "4":
        i = 4
        create_event(db)

    elif i == "0":
        main_menu(db)

    else:
        management_menu(db)


# =====================> DATABASE RELATED <========================

def edit_team(db, team_name):
    print("")
    print("WORKING IN {} TEAM".format(team_name.upper()))
    print("=======================================")
    print("[                                     ]")
    print("[     1· List players                 ]")
    print("[     2· Add player                   ]")
    print("[     3· Remove player                ]")
    print("[     0· Go back for the grace of god ]")
    print("[                                     ]")
    print("=======================================")
    i = input("Select an option (number of list): ")
    if i == "1":
        list_players(db, team_name, True)

    elif i == "2":
        x = list_players(db, team_name, False)
        if x < 7:
            add_player(db, team_name)
        else:
            print("This team is full")

    if i == "3":
        fire_player(db, team_name)

    elif i == "0":
        management_menu(db)

    else:
        edit_team(db, team_name)


def list_teams(db):
    col = db["team"]

    for t in col.find({}, {"_id": 0, "players": 0}):
        print(t)
    management_menu(db)


def list_players(db, team, show):
    col = db["team"]
    actual = []
    if show:
        print("Players:")
        for x in col.find({"name": team}):
            actual = x["players"]

        for p in actual:
            print(p)
    else:
        for p in col.find({"name": team}):
            actual = p["players"]

    total = len(actual)

    return total


def find_team(db, team):
    col = db["team"]
    teams = col.find({"name": team})

    if teams.count() > 0:
        return True
    else:
        print("Sorry, team not found")
        return False


def add_player(db, team):
    name = input("Name: ")
    nick = input("Nickname: ")
    col = db["team"]
    actual = ""

    for x in col.find({"name": team}):
        actual = x["players"]

    actual.append({"name": name, "nick": nick})

    query = {"name": team}
    add = {"$set": {"players": actual}}
    col.update(query, add)
    print("Welcome to the team {}".format(nick))
    edit_team(db, team)


def fire_player(db, team):
    name = input("Name: ")
    nick = input("Nickname: ")
    col = db["team"]
    actual = ""
    for x in col.find({"name": team}):
        actual = x["players"]

    actual.remove({"name": name, "nick": nick})

    print(actual)

    query = {"name": team}
    add = {"$set": {"players": actual}}
    col.update(query, add)
    print("May the force be with you {}".format(nick))
    edit_team(db, team)


def add_team(db):
    team = input("Team name: ")
    players = []
    add = input("Would you like to add players now:(y/n) ")
    if add.upper() == "Y":
        more = True
        i = 0
        while more:
            name = input("Player name: ")
            nick = input("Player nickname: ")
            players.append({"name": name, "nick": nick})
            i = i + 1
            if i < 7:
                add = input("Whould you like to add another player?(y/n)")
                if add.upper() == "N":
                    more = False
            else:
                print("Team is full")
                more = False
    col = db["team"]
    insert = {"name": team, "players": players}
    col.insert_one(insert)
    print("Welcome to the e-sports management system {}".format(team))


def find_events(db, only_name):
    collection = db["event"]
    events = []

    if not only_name:
        for event in collection.find():
            events.append({"startDate": event["startDate"], "endDate": event["endDate"],
                           "location": event["location"], "name": event["name"], "participants": event["participants"],
                           "prize": event["prize"]})
    else:
        for event in collection.find():
            events.append({"name": event["name"], "participants": event["participants"], "prize": event["prize"]})

    return events


def find_players(db):
    collection = db["player"]
    players = []

    for player in collection.find():
        players.append({"name": player["name"], "nick": player["nick"], "age": player["age"], "nac": player["nac"], "team": player["team"]})

    return players


def hall_of_fame(db):
    collection = db["team"]
    teams = []
    print("Here you will find the best teams that have passed through the most intense games of all time ;)")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    for team in collection.find():
        if len(team["prizes"]) > 0:
            print("{}, who have won ==> {}".format(team["name"], ", ".join(team["prizes"])))
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    main_menu(db)


def create_event(db):
    teams = db["team"]
    collection = db["event"]
    teamNames = []
    print("First of all select the event name...")
    name = input(">")
    print("When it's the event going to start? Please, use the following format (dd-mm-yyyy).")
    startDate = input(">")
    print("EndDate goes in here :)")
    endDate = input(">")
    print("Insert the location where the event will be.")
    location = input(">")
    print("All right, these is a treaky one, now the participants, let me show the available options: ")
    i = 1
    for team in teams.find():
        print("{}·{}".format(i, team["name"]))
        teamNames.append(team["name"])
        i += 1

    response = "-1"
    participants = []

    print("Now please write the corresponding number. Or press '0' to exit")
    while response != 0 and len(participants) < len(teamNames):
        response = int(input(">"))
        participants.append(teamNames[response - 1])
        if len(participants) == len(teamNames):
            print("Okey, you reached the maximun amount of teams available. Let's continue")
        else:
            print("Good decision, continue please...")

    print("And finally here comes the best part, write the event's prize.")
    prize = input(">")

    print("All right, that's everything, now let me do my thing...")

    collection.insert_one({"startDate": startDate, "endDate": endDate, "location": location, "name": name, "participants": participants, "prize": prize})


# =====================> OTHER LOGIC <========================
def filter_players_by_team(players, team):
    filtered = []

    for player in players:
        if player["team"] == team:
            filtered.append(player)

    return filtered


def show_events(db):
    onlyName = False
    events = find_events(db, onlyName)
    i = 1

    print("====================================")
    print("These are the events we have planned by now, I hope you enjoy every one of them!")
    print("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv")

    for event in events:
        print("{}· From {} to {} in {}, {} will fight for {} prize on the {} tournament!!".format(i, event[
            "startDate"], event["endDate"], event["location"], ", ".join(event["participants"]), event["prize"], event["name"]))
        print("====================================")
        i += 1


def start_event(db):
    onlyName = True
    events = find_events(db, onlyName)
    i = 1
    print("Which event do you want to start... ")
    for event in events:
        print("{}· {}".format(i, event["name"]))
        i += 1

    response = int(input("> "))

    event = events[response - 1]

    resolve_event(db, event)


def resolve_event(db, event):
    participants = event["participants"]
    collection = db["team"]
    players = find_players(db)

    print("{} participants are..".format(event["name"]))
    print("")

    for teamName in participants:
        print("{} ==> formed by:".format(teamName))
        teamPlayers = filter_players_by_team(players, teamName)

        for player in teamPlayers:
            print("{} [{}], who is {} years old. Also known as {}".format(player["name"], player["nac"], int(player["age"]), player["nick"]))

        print("")

    winners = participants

    while len(winners) != 1:
        winners = resolve_matches(winners)
        if len(winners) > 1:
            print("")
            print("After publicity we will see the next matches, don't go!")
            time.sleep(2.5)
            print("Aaaaaaaalll riiiiight!! We are back! ready to see more games? Let's go into it.")

    print("")

    print("THE TIME... HAS COOOME!!! The winner.. of this years {} torunament.. is ............ {} !!!!! Congratulations!")
    print(" We hope you enjoyed as much as us and see you in another event!".format(event["name"], winners[0]))

    prizes = (collection.find_one({"name": participants[0]})["prizes"])
    prizes.append(event["prize"])
    collection.update_one({"name": participants[0]}, {"$set": {"prizes": prizes}})

    main_menu(db)


def resolve_matches(participants):
    matches = int(len(participants) / 2)
    winners = []
    loosers = []

    for i in range(matches):
        winner = random.randint(0, len(participants) - 1)
        looser = random.randint(0, len(participants) - 1)

        while winner == looser:
            looser = random.randint(0, len(participants) - 1)

        winner = participants[winner]
        looser = participants[looser]

        print("On game number {}.. the winner is... {}!! An applause for {}. They also did very well!!".format(i + 1, winner, looser))

        winners.append(winner)
        loosers.append(looser)

        participants.remove(winner)
        participants.remove(looser)

    return winners


# =====================> MAIN <========================
mydb = create_connection("eSports")
main_menu(mydb)
